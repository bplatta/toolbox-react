/**
 * helper that increments integer keys of map
 * @param  {immutable.Map} imMap
 * @return {integer}
 */
export default function nextIndexFromMap(imMap) {
  return (parseInt(imMap.keySeq().max(), 10) || 0) + 1;
}
