/**
 * Maps numer of columns to MUICSS column class
 * @param  {integer} numberColumns
 * @return {String}
 */
export default function calculateMuiColumnCls(numberColumns) {
  return `mui-col-md-${Math.floor(12 / numberColumns)}`;
}
