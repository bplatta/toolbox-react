/**
 * Constructs object containing onEntry states for react routes
 * @param  {Object}  store - redux store
 * @param  {Function} isLoggedIn - takes redux state and returns Bool
 */
export default function redirectOnEntry(store, isLoggedIn) {
  return {
    requireAuth: (nextState, replace) => {
      const state = store.getState();
      if (!isLoggedIn(state)) replace('/login');
    },
    requireAnon: (nextState, replace) => {
      const state = store.getState();
      if (isLoggedIn(state)) replace('/users/me');
    }
  };
}
