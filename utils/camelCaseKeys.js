/* eslint-disable no-param-reassign */

/**
 * Converts keys of object to camel case
 * @param  {Object} obj
 * @return {Object}
 */
export default function camelCaseKeys(obj, splitOn='_') {
  return Object.keys(obj).reduce(
    (newObj, k) => {
      const parts = k.split(splitOn);
      let newKey;
      if (parts.length === 1) {
        newKey = parts[0];
      } else {
        newKey = parts[0] + parts
          .slice(1)
          .map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join('');
      }
      newObj[newKey] = obj[k];
      return newObj;
    },
    {}
  );
}
