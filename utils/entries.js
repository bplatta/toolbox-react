/**
 * Maps object to array of items
 * @param  {Object} obj
 * @return {string[][]}
 */
export default function entries(obj) {
  return Object.keys(obj).map((key) => [key, obj[key]]);
}
