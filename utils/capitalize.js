/**
 * Capitalize first letter of word
 * @param  {String} s
 * @return {String}
 */
export default function capitalize(s) {
  return s.charAt(0).toUpperCase() + s.substring(1);
}
