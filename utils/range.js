/**
 * Provides range function that can be used with `for`
 * @param  {integer} len
 * @return {integer[]}
 */
export default function range(len) {
  return [...Array(len)].map((_, i) => i);
}
