import entries from './entries';

/**
 * Maps object of classNames and bool values to string
 * @param  {Object} classesObject
 * @return {String}
 */
function classNamesToStr(classesObject) {
  let classString = '';
  const isActive = (active) => (
    (active !== undefined) && (
      (typeof (active) === 'boolean' && active === true) ||
      (typeof (active) === 'string' && active.length > 0)
    )
  );
  for (const [className, active] of entries(classesObject)) {
    if (isActive(active)) classString = classString.concat(' ', className);
  }
  return classString.trim();
}

export default classNamesToStr;
