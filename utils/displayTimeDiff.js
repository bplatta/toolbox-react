/**
 * Calculates the time difference between passed time and now
 * @param  {(string|datetime)} dateTime - datetime object or string to compare against
 * @return {string} human readable string
 */
export default function displayTimeDiff(dateTime) {
  const dateTimeObj = typeof (dateTime) === 'string' ? new Date(dateTime) : dateTime;
  const milliSecondsPassed = (new Date()) - dateTimeObj;
  const secondsPassed = milliSecondsPassed / 1000;
  const minutesPassed = secondsPassed / 60;
  const hoursPassed = minutesPassed / 60;
  const daysPassed = hoursPassed / 24;

  const nonZeroUnit = (timePassed) => Math.floor(timePassed) !== 0;

  if (nonZeroUnit(daysPassed)) {
    return `${Math.floor(daysPassed)} days`;
  } else if (nonZeroUnit(hoursPassed)) {
    return `${Math.floor(hoursPassed)} hours`;
  } else if (nonZeroUnit(minutesPassed)) {
    return `${Math.floor(minutesPassed)} minutes`;
  }
  return `${(Math.floor(secondsPassed) || 1)} seconds`;
}
