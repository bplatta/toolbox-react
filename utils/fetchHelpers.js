const defaultStatusCheck = (status) => status === 200;

/**
 * Simple Json parse handler for fetch
 * @param  {Object} response
 * @return {Object} valid JSON
 */
export const parseJSON = (response) => response.json();

/**
 * Construct Bearer auth header
 * @param  {String} token
 * @return {Object} header
 */
export const bearerAuthHeader = (token) => ({
  Authorization: `Bearer ${token}`
});

/**
 * Validate the API response status code, throw if invalid
 * @param  {Function} expectedStatus - function with signature (status) => Bool
 * @return {response|Error}
 */
export const checkStatus = (expectedStatus) => (response) => {
  const isValidStatus = expectedStatus || defaultStatusCheck;

  if (isValidStatus(response.status)) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
};

/**
 * Creates readable string from json response
 * @param  {Object} jsonError
 * @return {String}
 */
const convertJsonErrorToString = (jsonError) => {
  if (jsonError.error !== undefined) {
    return (
      Array.isArray(jsonError.error) ?
      jsonError.error.join('') :
      jsonError.error
    );
  }

  return Object.keys(jsonError).map((field) => (
    `${field}: ${JSON.stringify(jsonError[field])}.`
  )).join(' ');
};


/**
 * Helper function for retrieving token from state or requiring elevated
 * permissions
 * @param  {Object} state - redux state object
 * @return {Object} { headers: Object, requestPermissions: Bool }
 */
export const tokenOrElevate = (state) => {
  const apiToken = state.auth.get('apiToken');

  return (
    apiToken !== null ?
    { headers: bearerAuthHeader(apiToken), requestPermissions: false } :
    { headers: {}, requestPermissions: true });
};

/**
 * Parses the error string from the Error object response
 * @param  {Error} error
 * @return {Promise}
 */
export const parseAPIError = (error) => {
  // Error when the server does not respond
  if (error.name === 'TypeError' || error.code === 'ECONNREFUSED') {
    return Promise.resolve(
      'Connection refused... Uh oh, looks like the server is down.');
  // Error when 500 is returned
  } else if (error.message === 'Internal Server Error') {
    return Promise.resolve(
      'Looks like somethings wrong. We\'re looking into it.');
  } else if (error.message === 'Not Found') {
    return Promise.resolve(
      'Looks like somethings wrong. No endpoint exists for that request.');
  }

  // Expect a JSON object containing error data
  return error.response.json().then(convertJsonErrorToString);
};
