/**
 * Clean Url string
 * @param  {String} urlStr
 * @return {String}
 */
export default function cleanUrl(urlStr) {
  let cand = urlStr;
  if (!urlStr.startsWith('http')) {
    cand = `http://${urlStr}`;
  }
  return cand;
}
