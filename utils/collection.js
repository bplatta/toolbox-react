import { List, Map } from 'immutable';

/**
 * Constructs immutable collection from array of objects
 * @param  {Object[]} arrayOfObjects
 * @return {List[Map]}
 */
export default function collection(arrayOfObjects) {
  return List(arrayOfObjects.map((o) => Map(o)));
}
