/* eslint-disable no-return-assign*/

import React from 'react';

import Tooltip from '../Tooltip';

if (process.env.BROWSER) require('./styles.scss');

class FormWizardSection extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isValid: false,
      isDisabled: false
    };

    this._fields = {};

    this.isValid = this.isValid.bind(this);
  }

  isValid() {
    // call validate on each field, returns boolean: true if valid
    // search results for validation that returned false.
    const isValid = !(
      Object.keys(this._fields).map((fieldName) => {
        const validate = this._fields[fieldName].validate;
        return validate !== undefined ? validate() : true;
      }).includes(false));

    this.setState({ isValid: isValid });
    return isValid;
  }

  render() {
    const { title, fields, tooltip } = this.props;
    const activeClass = this.props.isActive ? 'active' : '';
    const tooltipComponentOrNull = (
      tooltip !== undefined ? <Tooltip {...tooltip} /> : null);
    return (
      <div className={`form-section ${activeClass}`.trim()}>
        <h4>{title}{tooltipComponentOrNull}</h4>
        {fields.map((field, index) => {
          const controlProps = {
            key: index,
            ref: (ref) => this._fields[`field${index}`] = ref
          };
          return React.cloneElement(field, Object.assign(controlProps, field.props));
        })}
      </div>
    );
  }
}

FormWizardSection.propTypes = {
  title: React.PropTypes.string.isRequired,
  tooltip: React.PropTypes.object,
  isActive: React.PropTypes.bool,
  fields: React.PropTypes.array.isRequired
};

FormWizardSection.defaultProps = {
  title: 'Form Section'
};

export default FormWizardSection;
