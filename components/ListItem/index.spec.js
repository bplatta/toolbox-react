import React from 'react';
import { shallow } from 'enzyme';
import ListItem from './index';

describe('toolbox/ListItem', () => {
  it('should render a list item if object passed', () => {
    const wrapper = shallow(<ListItem><h3>Only one object</h3></ListItem>);
    expect(wrapper.text().indexOf('Only one object')).not.toBe(-1);
  });

  it('should render a list item if array of children passed', () => {
    const wrapper = shallow(
      <ListItem>
        <div className="child-1">CHILD 1</div>
        <div className="child-2">CHILD 2</div>
      </ListItem>);
    expect(wrapper.find('.child-1').length).toEqual(1);
    expect(wrapper.find('.child-2').length).toEqual(1);
  });
});
