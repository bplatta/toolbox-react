import React from 'react';

if (process.env.BROWSER) require('./styles.scss');

/**
 * Simple ListItem to be used for consistent styling
 * @param {Object} props
 * @param {node} props.children - node object that can be rendered
 */
function ListItem(props) {
  const { children } = props;
  return (
    <div className="list-item mui-col-md-12">
      { children }
    </div>
  );
}

ListItem.propTypes = {
  children: React.PropTypes.node
};

export default ListItem;
