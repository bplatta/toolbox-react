/* eslint-disable no-unused-vars */

import React from 'react';

/**
 * Tab for Tabs container
 * @param {Object} props
 * @param {string} props.className
 * @param {string} props.label
 * @param {node} props.children - node object that can be rendered
 */
function Tab(props) {
  return (
    <div className={props.className} label={props.label}>{props.children}</div>
  );
}

Tab.propTypes = {
  className: React.PropTypes.string,
  label: React.PropTypes.string.isRequired,
  children: React.PropTypes.node.isRequired,
};

export default Tab;
