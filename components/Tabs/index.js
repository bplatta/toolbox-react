import React from 'react';
import Tab from './Tab';

if (process.env.BROWSER) require('./styles.scss');

class Tabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0
    };
    this.onTabClick = this.onTabClick.bind(this);
  }

  onTabClick(event) {
    this.setState({ currentIndex: parseInt(event.target.getAttribute('value'), 10) });
  }

  render() {
    const { children, barLocation, className } = this.props;
    const linkBarLocation = barLocation || 'top';
    const links = [];
    const content = [];
    const tabs = Array.isArray(children) ? children : [children];
    for (const [index, tab] of tabs.entries()) {
      const activeClass = index === this.state.currentIndex ? 'active' : '';
      links.push(
        <li key={index}>
          <a value={index} onClick={this.onTabClick}>
            {tab.props.label}
          </a>
        </li>
      );
      content.push(
        <div key={index} className={`tab-content ${activeClass} ${tab.props.className}`.trim()}>
          {tab.props.children}
        </div>
      );
    }

    return (
      <div className={`tabs-container mui-container ${className}`.trim()}>
        <div className="mui-row">
          <div className={`tabs-links-${linkBarLocation} mui-col-md-4`}>
            <ul>
              {links}
            </ul>
          </div>
          <div className="tabs-content-row mui-col-md-8">
            {content}
          </div>
        </div>
      </div>
    );
  }
}

Tabs.propTypes = {
  className: React.PropTypes.string,
  children: React.PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.instanceOf(Tab),
  ]),
  barLocation: React.PropTypes.oneOf(['top'])
};

export { Tabs as default, Tab };
