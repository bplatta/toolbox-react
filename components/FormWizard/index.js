import React from 'react';
import Button from 'muicss/lib/react/button';
import Form from 'muicss/lib/react/form';
import Loader from '../Loader';

if (process.env.BROWSER) require('./styles.scss');

/**
 * Component for validating a Form that has multiple parts (FormWizardSection)
 *   props:
 *     label:String - Name of the Form Wizard, displayed as the legend
 *     submitText:String - text to use for the submit button
 *     handleSubmit:Function - function that will be called on after validating form
 *     children:Array<Component>: array of components,
 *       typically FormWizardSection but optionally can be any
 *       component that implements an isValid() method and accepts
 *       an isActive prop.
 *         isActive - informs component if it is visible
 *         isValid() - validates sub-form data
 */
class FormWizard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSection: 0,
      sectionCount: props.children.length
    };

    // refs
    this._sections = {};

    // form submit
    this.handleSubmit = this.handleSubmit.bind(this);

    // State changers
    this.nextSection = this.nextSection.bind(this);
    this.backSection = this.backSection.bind(this);

    // State checkers
    this.sectionIsValid = this.sectionIsValid.bind(this);
    this.isFinalSection = this.isFinalSection.bind(this);
    this.isFirstSection = this.isFirstSection.bind(this);

    // Component creators
    this.backButton = this.backButton.bind(this);
    this.nextButton = this.nextButton.bind(this);
    this.submitButton = this.submitButton.bind(this);
    this.getSectionNav = this.getSectionNav.bind(this);
  }

  getSectionNav() {
    // only show next button if first section
    if (this.isFirstSection()) return [this.nextButton()];
    // show back and form submit button for final section
    else if (this.isFinalSection()) return [this.backButton(), this.submitButton()];
    // show back and next for wizard nav
    return [this.backButton(), this.nextButton()];
  }

  backButton() {
    return <Button key="1" variant="flat" onClick={this.backSection}>Back</Button>;
  }

  nextButton() {
    return <Button key="2" variant="flat" onClick={this.nextSection}>Next</Button>;
  }

  submitButton() {
    return <Button key="3 "variant="flat" className="form-wizard-submit">{this.props.submitText}</Button>;
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.sectionIsValid()) this.props.handleSubmit(e);
  }

  sectionIsValid() {
    return this._sections[`section${this.state.currentSection}`].isValid();
  }

  isFinalSection() {
    return this.state.currentSection === this.state.sectionCount - 1;
  }

  isFirstSection() {
    return this.state.currentSection === 0;
  }

  backSection(e) {
    e.preventDefault();
    const lastSection = this.state.currentSection;
    this.setState({ currentSection: lastSection - 1 });
  }

  nextSection(e) {
    e.preventDefault();
    if (this.sectionIsValid()) {
      const lastSection = this.state.currentSection;
      this.setState({ currentSection: lastSection + 1 });
    }
  }

  render() {
    const { label, children, requestInProgress } = this.props;
    return (
      <div className="form-wizard-container">
        <Form className="form-wizard" label={label} onSubmit={this.handleSubmit} noValidate>
          <legend>{label}</legend>
          {children.map(
            (section, index) => {
              const controlProps = {
                key: index,
                isActive: this.state.currentSection === index,
                ref: (ref) => this._sections[`section${index}`] = ref
              };
              return React.cloneElement(
                section, Object.assign(controlProps, section.props)
              );
            }
          )}
          <div className="form-nav-buttons">
            {this.getSectionNav()}
            <span>
              <Loader active={requestInProgress} />
            </span>
          </div>
        </Form>
      </div>
    );
  }
}

/**
 * Accepted props for a wizard
 * @param {String} label - legend text
 * @param {String} submitText - text for the submit button, default Submit
 * @param {Function} handleSubmit - function called on valid submit
 * @param {Component[]} children - Array of components that satisify the following
 *     1 - accepts isActive<Bool> prop to determine if visible
 *     2 - implements isValid -> <Bool> function to determine if section valid
 */
FormWizard.propTypes = {
  label: React.PropTypes.string.isRequired,
  submitText: React.PropTypes.string,
  handleSubmit: React.PropTypes.func.isRequired,
  children: React.PropTypes.array.isRequired,
  requestInProgress: React.PropTypes.bool
};

FormWizard.defaultProps = {
  submitText: 'Finish'
};

export default FormWizard;
