import React from 'react';
import Tooltip from '../Tooltip';
import classNamesToStr from '../../utils/classNames';

if (process.env.BROWSER) require('./styles.scss');

/**
 * Input component that provides public validate function for
 * using DOM validate API
 */
class EnhancedInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isInvalid: false,
      wasInFocus: false,
      error: ''
    };

    this.validate = this.validate.bind(this);
    this.shouldCleanProtocol = this.shouldCleanProtocol.bind(this);

    // event handlers
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleFocus() {
    this.setState({ isInvalid: false, wasInFocus: true });
  }

  handleBlur() {
    this.validate();
  }

  shouldCleanProtocol(value) {
    return this.props.type === 'url' && value !== '' && !value.startsWith('http');
  }

  /**
   * Validate dom value. Returns true if valid, false if invalid
   * @return {Boolean} - indicating if valid
   */
  validate() {
    if (this.domRef) {
      if (this.shouldCleanProtocol(this.domRef.value)) {
        this.domRef.value = `http://${this.domRef.value}`;
      }
      const isValid = this.domRef.checkValidity();

      this.setState({
        wasInFocus: true,
        isInvalid: !isValid,
        error: this.domRef.validationMessage
      });

      return isValid;
    }

    return true;
  }

  render() {
    const {
      type,
      tooltip,
      required,
      placeholder,
      value,
      disabled,
      className,
      ...rest
    } = this.props;
    const isEmpty = value.length === 0;
    const showErrorClass = this.state.isInvalid && !disabled ? 'show' : '';
    const classNames = {
      'non-empty': !isEmpty
    };
    // add classname if passed
    if (className !== undefined) classNames[className] = true;

    const allProps = Object.assign({
      type,
      value,
      required,
      disabled,
      ref: (ref) => this.domRef = ref,
      className: classNamesToStr(classNames),
      onFocus: this.handleFocus,
      onBlur: this.handleBlur
    }, rest);

    return (
      <div className="input-field">
        {(
          type === 'textarea' ?
            <textarea {...allProps} /> : <input {...allProps} />
        )}
        <span className="bar" />
        <label>{placeholder}</label>
        {(
          tooltip !== undefined ?
            <Tooltip className="input-tooltip" {...tooltip} /> :
            null
        )}
        <span className={`error ${showErrorClass}`.trim()}>{this.state.error}</span>
      </div>
    );
  }
}

/**
 * EnhancedInput PropTypes
 * @type {Object} props
 * @type {Object} props.value - state value
 * @type {boolean} props.required - indicating if input is required
 * @type {string} props.placeholder
 * @type {string} props.type - type of input field
 */
EnhancedInput.propTypes = {
  value: React.PropTypes.node.isRequired,
  disabled: React.PropTypes.bool,
  tooltip: React.PropTypes.object,
  required: React.PropTypes.bool,
  className: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  type: React.PropTypes.string
};

EnhancedInput.defaultProps = {
  required: false,
  type: 'text'
};

export default EnhancedInput;
