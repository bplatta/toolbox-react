import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import EnhancedInput from './index';

describe('toolbox/EnhancedInput', () => {
  it('should match snapshot', () => {
    const component = renderer.create(<EnhancedInput className="en-in" value="" />);
    let tree = component.toJSON();
    expect(tree.type).toBe('div');
    expect(tree).toMatchSnapshot();

    tree.children[0].props.onFocus();
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    tree.children[0].props.onBlur();
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should handle render for textarea type', () => {
    const wrapper = shallow(<EnhancedInput type="textarea" value="" />);
    expect(wrapper.find('textarea').length).toBe(1);
    expect(wrapper.find('.bar').length).toBe(1);
    expect(wrapper.find('.error').length).toBe(1);
    expect(wrapper.find('label').length).toBe(1);
    expect(wrapper.find('textarea').hasClass('non-empty')).toBe(false);
  });

  it('should handle render for input type', () => {
    const wrapper = shallow(<EnhancedInput type="text" value="" />);
    expect(wrapper.find('input').length).toBe(1);
    expect(wrapper.find('.bar').length).toBe(1);
    expect(wrapper.find('.error').length).toBe(1);
    expect(wrapper.find('label').length).toBe(1);
    expect(wrapper.find('input').hasClass('non-empty')).toBe(false);
  });
});
