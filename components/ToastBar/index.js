import { Map } from 'immutable';
import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Toast from '../Toast';

if (process.env.BROWSER) require('./styles.scss');

class ToastBar extends React.Component {
  constructor(props) {
    super(props);
    this.removeToast = this.removeToast.bind(this);
  }

  removeToast(toastIndex) {
    // Curried action to pass to Toast component
    return () => this.props.removeToast(toastIndex);
  }

  render() {
    const { toasts } = this.props;
    const nextToast = this.props.nextToast(toasts);

    const content = [];
    if (nextToast !== null) {
      content.push(
        <Toast
          key={nextToast.id}
          removeToast={this.removeToast(nextToast.id)}
          {...nextToast}
        />
      );
    }

    return (
      <div className="toast-bar mui-container-fluid">
        <ReactCSSTransitionGroup
          transitionName="toast"
          transitionEnterTimeout={300}
          transitionLeaveTimeout={150}
        >
          {content}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

ToastBar.propTypes = {
  toasts: React.PropTypes.instanceOf(Map),
  removeToast: React.PropTypes.func.isRequired,
  nextToast: React.PropTypes.func.isRequired
};

export default ToastBar;
