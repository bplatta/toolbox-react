import React from 'react';

import EnhancedInput from '../EnhancedInput';
import SwitchBox from '../SwitchBox';

if (process.env.BROWSER) require('./styles.scss');

class SwitchInput extends React.Component {
  constructor(props) {
    super(props);

    this.input = null;
    this.validate = this.validate.bind(this);
  }

  validate() {
    // Validate input field if it is visible
    if (this.props.showInput === true) {
      return this.input.validate();
    }

    return true;
  }

  render() {
    const { inputAttr, switchAttr, showInput } = this.props;
    const activeClass = showInput ? 'show' : '';
    return (
      <span className={`switch-input ${activeClass}`.trim()}>
        <SwitchBox {...switchAttr} />
        <EnhancedInput disabled={!showInput} ref={(ref) => this.input = ref} {...inputAttr} />
      </span>
    );
  }
}

SwitchInput.propTypes = {
  inputAttr: React.PropTypes.object.isRequired,
  switchAttr: React.PropTypes.object.isRequired,
  showInput: React.PropTypes.bool.isRequired
};

export default SwitchInput;
