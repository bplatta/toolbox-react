import React from 'react';
import { Map, List, OrderedMap } from 'immutable';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Button from 'muicss/lib/react/button';

import EnhancedInput from '../EnhancedInput';
import { calculateColumnCls, capitalize } from '../../utils';

if (process.env.BROWSER) require('./styles.scss');

function validateInputList(inputList) {
  const isInvalid = inputList.map((input) => {
    const validate = input.validate;
    return validate !== undefined ? validate() : true;
  }).includes(false); // indicates at least one input is invalid

  // return false if invalid
  return !isInvalid;
}


class AdditiveInputList extends React.Component {
  constructor(props) {
    super(props);

    this.shouldComponentUpdate = this.shouldComponentUpdate.bind(this);
    this.isValid = this.isValid.bind(this);
    this.mapElementToRow = this.mapElementToRow.bind(this);
    this.addNewElement = this.addNewElement.bind(this);
    this.updateElement = this.updateElement.bind(this);
    this.removeElement = this.removeElement.bind(this);
    this._allInputs = Map({});
  }

  shouldComponentUpdate(nextProps) {
    return !(this.props.elements.equals(nextProps.elements));
  }

  addNewElement(e) {
    e.preventDefault();
    this.props.addElement();
  }

  updateElement(index) {
    return (fieldName) => (e) => {
      e.preventDefault();
      this.props.updateElement(index)(fieldName)(e.target.value);
    };
  }

  removeElement(index) {
    return (e) => {
      e.preventDefault();
      this._allInputs = this._allInputs.delete(index);
      this.props.removeElement(index);
    };
  }

  isValid() {
    const inputsValidated = this._allInputs.valueSeq().toArray().map(
      (elementInputList) => validateInputList(elementInputList)
    );

    // false indicates that one row was invalid
    const isInvalid = inputsValidated.includes(false);

    return !isInvalid;
  }

  mapElementToRow(elementTypeDef, columnCls) {
    // Element is expected to be a immutable.Map
    return ([index, element]) => {
      const indexStr = index.toString();
      if (!this._allInputs.has(indexStr)) {
        this._allInputs = this._allInputs.set(indexStr, List([]));
      }

      return (
        <div key={`in-list-${index}`} className="mui-row">
          {elementTypeDef.keySeq().toArray().map((fieldName) => (
            <div key={`${index}-${fieldName}`} className={columnCls}>
              <EnhancedInput
                required
                type={elementTypeDef.get(fieldName)}
                value={element.get(fieldName)}
                onChange={this.updateElement(indexStr)(fieldName)}
                ref={
                  (ref) => { // map input elements to list handle for validation
                    if (ref && this._allInputs.get(indexStr).size < elementTypeDef.size) {
                      this._allInputs = this._allInputs.updateIn(
                        [indexStr], (inputRefs) => inputRefs.push(ref)
                      );
                    }
                  }
                }
              />
            </div>
          ))}
          <a onClick={this.removeElement(indexStr)}>
            <span className="delete-element ti-trash" />
          </a>
        </div>
      );
    };
  }

  render() {
    const { elements, elementTypeDef } = this.props;
    const columnCls = calculateColumnCls(elementTypeDef.size);
    const columnTitles = elementTypeDef.keySeq().map(
      (fieldName) => (
        <div key={`title-key-${fieldName}`} className={columnCls}>
          <h5>{capitalize(fieldName)}</h5>
        </div>
      )
    );
    const mapElementToRow = this.mapElementToRow(elementTypeDef, columnCls);
    return (
      <div className="additive-list mui-container-fluid">
        <div className="mui-row column-titles">
          { columnTitles }
        </div>
        <ReactCSSTransitionGroup
          transitionName="add-list"
          transitionEnterTimeout={150}
          transitionLeaveTimeout={300}
        >
          { elements.entrySeq().toArray().map(mapElementToRow) }
        </ReactCSSTransitionGroup>
        <div className="mui-row add-element-row">
          <Button variant="fab" onClick={this.addNewElement}><span className="ti-plus" /></Button>
        </div>
      </div>
    );
  }
}

AdditiveInputList.propTypes = {
  elements: React.PropTypes.instanceOf(Map).isRequired,
  elementTypeDef: React.PropTypes.instanceOf(OrderedMap).isRequired,
  removeElement: React.PropTypes.func.isRequired,
  addElement: React.PropTypes.func.isRequired,
  updateElement: React.PropTypes.func.isRequired
};

export default AdditiveInputList;
