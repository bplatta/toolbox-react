import React from 'react';

if (process.env.BROWSER) require('./styles.scss');

function Tooltip(props) {
  const { tipText, iconClass, direction, className } = props;
  const classNameFinal = `${iconClass} tooltip--${direction} ${className || ''}`.trim();
  return (
    <div className="tt-span">
      <span className={classNameFinal} data-tooltip={tipText} />
    </div>
  );
}

Tooltip.propTypes = {
  className: React.PropTypes.string,
  tipText: React.PropTypes.string.isRequired,
  iconClass: React.PropTypes.string,
  direction: React.PropTypes.oneOf(['bottom', 'right', 'left'])
};

Tooltip.defaultProps = {
  iconClass: 'ti-help-alt',
  direction: 'bottom'
};

export default Tooltip;
