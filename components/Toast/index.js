import React from 'react';

if (process.env.BROWSER) require('./styles.scss');

class Toast extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      burnt: false
    };

    this.burnToast = this.burnToast.bind(this);
    this.setAutoHide = this.setAutoHide.bind(this);
    this.getIconClass = this.getIconClass.bind(this);
  }

  setAutoHide(delay) {
    setTimeout(() => {
      this.burnToast();
    }, delay);
  }

  getIconClass(type) {
    if (this.props.iconClass) return this.props.iconClass;

    if (type === 'info') return 'ti-info-alt';
    if (type === 'error') return 'ti-alert';
    if (type === 'success') return 'ti-check';
    return '';
  }

  burnToast() {
    this.props.removeToast();
  }

  render() {
    const { message, type, delay } = this.props;

    this.setAutoHide(delay);

    return (
      <div className="mui-row">
        <div className={`toast mui-col-md-6 mui-col-md-offset-3 ${type}`.trim()}>
          <span className={`toast-icon ${this.getIconClass(type)}`} />
          { message }
        </div>
      </div>
    );
  }
}

Toast.propTypes = {
  message: React.PropTypes.string.isRequired,
  type: React.PropTypes.oneOf(['error', 'success', 'info']).isRequired,
  delay: React.PropTypes.number,
  removeToast: React.PropTypes.func.isRequired,
  iconClass: React.PropTypes.string
};

Toast.defaultProps = {
  delay: 3500
};

export default Toast;
