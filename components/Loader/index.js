import React from 'react';

if (process.env.BROWSER) require('./styles.scss');

function Loader(props) {
  const activeClass = props.active ? 'active' : '';

  return (
    <div className={`loader-container ${activeClass}`.trim()}>
      <div className="ball-beat">
        <div />
        <div />
        <div />
      </div>
    </div>
  );
}

Loader.propTypes = {
  active: React.PropTypes.bool.isRequired
};

export default Loader;
