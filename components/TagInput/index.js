import React from 'react';
import { Map } from 'immutable';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Button from 'muicss/lib/react/button';
import Tag from '../Tag';

if (process.env.BROWSER) require('./styles.scss');

class TagsInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      nextTag: ''
    };

    this.shouldComponentUpdate = this.shouldComponentUpdate.bind(this);
    this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
    this.onTagInputChange = this.onTagInputChange.bind(this);
    this.addTag = this.addTag.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.tags.size > this.props.tags.size) {
      // reset set if new tags are passed
      this.setState({ nextTag: '' });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state.nextTag !== nextState.nextTag ||
      !nextProps.tags.equals(this.props.tags)
    );
  }

  onTagInputChange(e) {
    this.setState({
      nextTag: e.target.value
    });
  }

  onKeyUp(e) {
    if (e.keyCode === 13) {
      this.addTag(e);
    }
  }

  addTag(e) {
    e.preventDefault();
    if (this.state.nextTag !== '') {
      this.props.addTag(this.state.nextTag);
    }
  }

  render() {
    const { tags, removeTag } = this.props;

    const nonEmptyCls = this.state.nextTag !== '' ? 'non-empty' : '';
    return (
      <div className="tags-box mui-container-fluid">
        <div className="mui-row">
          <div className="mui-col-md-4 tag-input">
            <input
              className={`${nonEmptyCls}`}
              onChange={this.onTagInputChange}
              value={this.state.nextTag}
              onKeyUp={this.onKeyUp}
              type="text"
            />
            <span className="bar" />
            <label>tag</label>
            <Button
              variant="flat"
              onClick={this.addTag}
            >
              <span className="ti-plus" />
            </Button>
          </div>
          <div className="mui-col-md-8 tags-display">
            <ReactCSSTransitionGroup
              transitionName="tag-list"
              transitionEnterTimeout={150}
              transitionLeaveTimeout={300}
            >
              {tags.entrySeq().toArray().map(
                ([index, text]) => (
                  <Tag key={index} removeTag={removeTag(index)}>{text}</Tag>
                )
              )}
            </ReactCSSTransitionGroup>
          </div>
        </div>
      </div>
    );
  }
}

TagsInput.propTypes = {
  tags: React.PropTypes.instanceOf(Map).isRequired,
  addTag: React.PropTypes.func.isRequired,
  removeTag: React.PropTypes.func
};

export default TagsInput;
