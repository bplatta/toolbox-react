import React from 'react';
import { shallow } from 'enzyme';
import NavBar from './index';

describe('toolbox/NavBar', () => {
  it('should render array of children into list', () => {
    const wrapper = shallow(<NavBar><div>element1</div><div>element2</div></NavBar>);
    expect(wrapper.find('ul').children().length).toBe(2);
    const text = wrapper.text();
    expect(text.indexOf('element1')).not.toBe(-1);
    expect(text.indexOf('element2')).not.toBe(-1);
  });

  it('should render child object into list', () => {
    const wrapper = shallow(<NavBar><div>Not an array</div></NavBar>);
    expect(wrapper.find('ul').children().length).toBe(1);
  });
});
