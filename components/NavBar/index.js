import React from 'react';

if (process.env.BROWSER) require('./styles.scss');

/**
 * Navbar component for consistent styling
 * @param {Object} props
 * @param {string} props.className
 * @param {(array|object)} props.children - components to be mapped to <li> elements
 */
function NavBar(props) {
  const { className, children } = props;

  return (
    <div className={`navbar ${className}`.trim()}>
      <ul>
        {React.Children.map(children, (child, index) => {
          if (child.type !== 'li') return <li key={index}>{child}</li>;
          return child;
        })}
      </ul>
    </div>
  );
}

NavBar.propTypes = {
  children: React.PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.object,
  ]),
  className: React.PropTypes.string
};

NavBar.defaultProps = {
  className: ''
};

export default NavBar;
