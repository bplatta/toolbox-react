import React from 'react';

if (process.env.BROWSER) require('./styles.scss');

class SwitchBox extends React.Component {
  render() {
    const { onLabel, offLabel, className, ...rest } = this.props;
    const finalClassName = `hide ${className || ''}`.trim();
    const thisId = onLabel.replace(/ /g, '_').toLowerCase();
    return (
      <div className="switch-box">
        <span>{onLabel}</span>
        <input
          id={thisId}
          label="Public"
          type="checkbox"
          className={finalClassName}
          {...rest}
        />
        <label htmlFor={thisId} />
        <span>{offLabel}</span>
      </div>
    );
  }
}

SwitchBox.propTypes = {
  onLabel: React.PropTypes.string.isRequired,
  offLabel: React.PropTypes.string.isRequired,
  className: React.PropTypes.string
};

export default SwitchBox;
