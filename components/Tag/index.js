import React from 'react';

if (process.env.BROWSER) require('./styles.scss');

function Tag(props) {
  const { className, children, removeTag } = props;
  const deleteButton = (
    removeTag !== undefined ?
      <a onClick={removeTag}><span className="ti-trash" /></a> :
      ''
  );
  return (
    <span className={`tag ${className || ''}`.trim()}>
      { children }
      {deleteButton}
    </span>
  );
}

Tag.propTypes = {
  className: React.PropTypes.string,
  children: React.PropTypes.string,
  removeTag: React.PropTypes.func
};

export default Tag;
