import React from 'react';
import Button from 'muicss/lib/react/button';
import Form from 'muicss/lib/react/form';
import range from '../../utils/range';
import Loader from '../Loader';

if (process.env.BROWSER) require('./styles.scss');

class EnhancedForm extends React.Component {
  constructor(props) {
    super(props);
    this._fields = {};
    this.validate = this.validate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    if (this.validate()) this.props.handleSubmit(e);
  }

  validate() {
    // call validate on each field, returns boolean: true if valid
    // search results for validation that returned false.
    // If it doesnt exist (undefined), then fields are valid!
    return range(this.props.children.length)
      .map((fieldIndex) => this._fields[`field${fieldIndex}`].validate())
      .find((isClean) => isClean === false) === undefined;
  }

  render() {
    const {
      label,
      children,
      submitText,
      crossFieldErrors,
      requestInProgress
    } = this.props;
    const showCrossFieldClass = (
      crossFieldErrors !== undefined && crossFieldErrors !== '' ? 'show' : '');

    return (
      <Form className="enhanced-form" label={label} onSubmit={this.onSubmit} noValidate>
        <legend>{label}</legend>
        {children.map((inputField, index) => {
          const controlProps = {
            key: index,
            ref: (ref) => this._fields[`field${index}`] = ref
          };
          return React.cloneElement(
            inputField,
            Object.assign(controlProps, inputField.props));
        })}
        <div className="bottom-form-control">
          <Button variant="flat" className="submit-button">{submitText}</Button>
          <span className={`cross-field-errors ${showCrossFieldClass}`.trim()}>
            {crossFieldErrors}
          </span>
          <span>
            <Loader active={requestInProgress || false} />
          </span>
        </div>
      </Form>
    );
  }
}

/**
 * Configurable props for Enhanced input components
 * validators:
 *   Array of funcs:
 *     () -> ({ isValid: bool, error: str })
 * @type {Object}
 */
EnhancedForm.propTypes = {
  label: React.PropTypes.string,
  children: React.PropTypes.array.isRequired,
  submitText: React.PropTypes.string,
  crossFieldErrors: React.PropTypes.string,
  handleSubmit: React.PropTypes.func.isRequired,
  requestInProgress: React.PropTypes.bool
};

EnhancedForm.defaultProps = {
  label: 'Form',
  submitText: 'Submit',
  requestInProgress: false
};

export default EnhancedForm;
