import React from 'react';
import EnhancedForm from '../EnhancedForm';
import EnhancedInput from '../EnhancedInput';

/**
 * Component encapsulating login logic
 */
class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.onLoginSubmit(this.state.username, this.state.password);
  }

  handleFieldChange(field) {
    return (e) => {
      this.setState({ [field]: e.target.value });
    };
  }

  render() {
    const { className, ...rest } = this.props;
    return (
      <EnhancedForm
        label="Login"
        className={`login-form ${className}`.trim()}
        handleSubmit={this.handleSubmit}
        {...rest}
      >
        <EnhancedInput
          className="login-form-username"
          value={this.state.username}
          placeholder="Email"
          onChange={this.handleFieldChange('username')}
          type="email"
          required
        />
        <EnhancedInput
          className="login-form-password"
          ref={(ref) => this.passwordRef = ref}
          value={this.state.password}
          type="password"
          placeholder="Password"
          onChange={this.handleFieldChange('password')}
          required
        />
      </EnhancedForm>
    );
  }
}

/**
 * Props for Login component
 * @type {Object}
 * @type {string} className
 * @type {function} onLoginSubmit - function that accepts (username, password)
 */
LoginForm.propTypes = {
  className: React.PropTypes.string,
  onLoginSubmit: React.PropTypes.func.isRequired
};

LoginForm.defaultProps = {
  className: ''
};

export default LoginForm;
