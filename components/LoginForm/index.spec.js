import React from 'react';
import { shallow, mount } from 'enzyme';
import LoginForm from './index';

describe('toolbox/LoginForm', () => {
  it('should render form', () => {
    const onLoginSubmit = jest.fn();
    const wrapper = shallow(<LoginForm onLoginSubmit={onLoginSubmit} />);
    expect(wrapper.find('.login-form').length).toBe(1);
    expect(wrapper.find('.login-form-username').length).toBe(1);
    expect(wrapper.find('.login-form-password').length).toBe(1);
  });

  it('should setState on username and password change events', () => {
    const onLoginSubmit = jest.fn();
    const wrapper = mount(<LoginForm onLoginSubmit={onLoginSubmit} />);
    wrapper.find('.login-form-username').simulate(
      'change', { target: { value: 'mario' } });
    wrapper.find('.login-form-password').simulate(
      'change', { target: { value: 'rottenpeach' } });

    expect(wrapper.state()).toEqual({
      username: 'mario',
      password: 'rottenpeach'
    });
  });
});
