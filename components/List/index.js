import React from 'react';
import Immutable from 'immutable';

if (process.env.BROWSER) require('./styles.scss');

/**
 * Simple component for rendering list of items with consistent styling
 */
class List extends React.Component {
  render() {
    const { items, transformItem, emptyText, className } = this.props;

    const finalItems = items.map(transformItem);

    let content;
    if (finalItems && finalItems.size !== 0) {
      content = finalItems;
    } else {
      content = (
        <div className="empty-list">
          <h3>{ emptyText }</h3>
        </div>
      );
    }

    return (
      <div className={`item-list mui-row ${className}`.trim()}>
        { content }
      </div>
    );
  }
}

/**
 * Accepts items which can be html elements or objects that require
 * a transform, which can be passed as well
 * @type {Object}
 * @type {List} items - Immutable list of items to be
 * @type {function} itemsTransform - function that maps item to element that can be rendered
 * @type {string} emptyText - text to display if list is empty
 * @type {string} className - optinoal classname
 */
List.propTypes = {
  items: React.PropTypes.instanceOf(Immutable.List).isRequired,
  transformItem: React.PropTypes.func.isRequired,
  emptyText: React.PropTypes.string,
  className: React.PropTypes.string
};

List.defaultProps = {
  emptyText: 'Hm looks like this list is empty!'
};

export default List;
