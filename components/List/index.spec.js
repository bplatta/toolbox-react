import React from 'react';
import { shallow, render } from 'enzyme';
import collection from '../../utils/collection';
import List from './index';

describe('toolbox/List', () => {
  // default item transform
  const transform = (item) => <div key={item.get('id')}>{item.get('content')}</div>;

  it('should return empty-list div with no children', () => {
    const emptyText = 'Is Empty';
    const wrapper = shallow(
      <List items={collection([])} emptyText={emptyText} transformItem={transform} />);
    expect(wrapper.text().indexOf(emptyText)).not.toBe(-1);
  });

  it('should render children', () => {
    const items = [{ id: 1, content: '1' }, { id: 2, content: '2' }, { id: 3, content: '3' }];
    const wrapper = shallow(
      <List items={collection(items)} transformItem={transform} />);
    expect(wrapper.children().length).toEqual(3);
  });

  it('should return transformed children with transform function', () => {
    const items = [{ id: 1 }, { id: 2 }];
    const transformItem = (item) => (
      <div key={item.get('id')} className={`item-transform-${item.get('id')}`}>
        transformed item
      </div>);
    const wrapper = render(<List items={collection(items)} transformItem={transformItem} />);
    expect(wrapper.find('.item-transform-1').length).toEqual(1);
    expect(wrapper.find('.item-transform-2').length).toEqual(1);
  });
});
